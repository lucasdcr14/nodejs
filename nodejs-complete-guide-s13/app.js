const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const errorController = require('./controllers/error');
const User = require('./models/user');

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
  User.findById('5c8949e77a91be1c9d9e10ed')
    .then(user => {
      req.user = user;
      next();
    })
    .catch(err => console.log(err));
});

app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

mongoose.connect('mongodb+srv://lucasdcr14:fqk0QdMvlGGnRPbY@cluster0-mu0ng.mongodb.net/shop?retryWrites=true')
  .then(result => {    
    User.findOne().then(user => {
      if(!user) {
        const user = new User({
          name: 'max',
          email: 'max@test.com',
          cart: {
            items: []
          }
        });

        user.save()
      }
    })
        
    app.listen(3000);
    console.log('Litenning in port 3000');
  })
  .catch(err => {
    console.log(err);
  });