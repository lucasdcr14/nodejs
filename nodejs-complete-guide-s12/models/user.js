const mongodb = require('mongodb');
const getDb = require('../util/database').getDb;

const ObjectID = mongodb.ObjectID;

class User {
  constructor(name, email, cart, id) {
    this.name = name;
    this.email = email;
    this.cart = cart; // {items: []}
    this._id = id;
  }

  save() {
    const db = getDb();
    return db.collection('users').insertOne(this)
      .then(result => {
        console.log(result);
      })
      .catch(err => {
        console.log(err);
      });
  }

  static findById(userId) {
    const db = getDb();
    console.log(userId);
    return db.collection('users').findOne({ _id: new ObjectID(userId) })
      .then(user => {
        console.log(user);
        return user;
      })
      .catch(err => {
        console.log(err);
      });
  }

  addToCart(product) {
    console.log('product>>>>>>>', product);
    console.log('cart>>>>>>>', this.cart);
    const cartProductIndex = this.cart.itens.findIndex(cp => {
      return cp.productId.toString() === product._id.toString();
    });
    let newQuantity = 1;
    const updatedCartItens = [...this.cart.itens]

    if (cartProductIndex >= 0) {
      newQuantity += this.cart.itens[cartProductIndex].quantity + 1;
      updatedCartItens[cartProductIndex].quantity = newQuantity;
    } else {
      updatedCartItens.push({ productId: new ObjectID(product._id), quantity: newQuantity });
    }

    const updatedCart = { itens: updatedCartItens };
    const db = getDb();
    return db.collection('users').updateOne({ _id: new ObjectID(this._id) }, { $set: { cart: updatedCart } })
  }

  getCart() {
    const db = getDb();
    const productIds = this.cart.itens.map(i => {
      return i.productId;
    });
    console.log("ids>>>>>>>>>>>>", productIds);
    return db.collection('products').find({_id: {$in: productIds}}).toArray()
      .then(products => {
        return products.map(p => {
          return {...p, quantity: this.cart.itens.find(i => {
            return i.productId.toString() === p._id.toString();
          }).quantity}
        })
      });
  } 

  deleteItemFromCart(productId) {
    const updatedCartItems = this.cart.itens.filter(i => {
      return i.productId.toString() !== productId.toString()
    });
    const db = getDb();
    return db.collection('users').updateOne({_id: new ObjectID(this._id)}, {$set: {cart: {itens: updatedCartItems}}});
  }

  addOrder() {
    const db = getDb();
    return this.getCart().then(products => {
      const order = {
        itens: products,
        user: {
          _id: new ObjectID(this._id),
          name: this.name,
          email: this.email
        }
      };
      return db.collection('orders').insertOne(order)
    }).then(result => {
        this.cart = {itens: []};
        return db.collection('users').updateOne({_id: new ObjectID(this._id)}, {$set: {cart: {itens: []}}});
      });
  }

  getOrders() {
    const db = getDb();
    return db.collection('orders').find({'user._id': new ObjectID(this._id)}).toArray();
  }
}

module.exports = User;
