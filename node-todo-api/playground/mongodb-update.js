//  const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, client) => {
    
    if(err) {
        return console.log('Unable to connect to MongoDB server.');
    }

    console.log('Connected to MongoDB server.');    
    const db = client.db('TodoApp');

    /* db.collection('Todos').findOneAndUpdate(
        {_id: new ObjectID('5bd333e5e5fe9d6a1d783a1c')}, 
        {$set: {completed: true}},
        {returnOriginal: false}).then((result) => {
            console.log(result);
        }); */

    db.collection('Users').findOneAndUpdate(
        {_id: new ObjectID('5bd32116a5eeec176896a05f')},
        {$set: {name: 'Lucas'}, $inc: {age: 1}},
        {returnOriginal: false}).then((result) => {
            console.log(result);
        });

    client.close();
})