const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const {ObjectID} = require('mongodb');

const {mongoose} = require('./db/mongoose');
const {Todo} = require('./models/todo');
const {User} = require('./models/user');

const app = express();

app.use(bodyParser.json());

app.post('/todos', (req, res) => {
    var todo = new Todo({
        text: req.body.text
    });

    todo.save().then((doc) => {
        res.send(doc);
    }, (e) => {
        res.status(400).send(e);
    });
});

app.get('/todos', (req, res) => {
    Todo.find().then((todos) => {
        res.send({todos});
    }, (e) => {
        res.status(400).send(e);
    });
});

app.get('/todos/:id', (req, res) => {
    var id = req.params.id;

    // valid id using isvalid
        // 404 - send back empty send
    // findById
        // sucess
            // if todo - send it back
            // if no todo - send back 404 with empty body
        // error
            // 400 - and send empty body back 

    if(!ObjectID.isValid(id)) {
        res.status(404).send();
    }

    Todo.findById(id).then((todo) => {
        if(!todo) {
            res.status(404).send()
        } else {
            res.send({todo});
        }
    }).catch((e) => res.status(400).send())
});

app.delete('/todos/:id', (req, res) => {
    // get the id

    // validate the id -> not valid? return 404

    // remove todo by id
        // sucess
            // if no doc, send 404
            // if doc, send dock back woth 200
        // error
            // 400 with empty body

    var id = req.params.id;

    if(!ObjectID.isValid(id)) {
        res.status(404).send();
    }

    Todo.findByIdAndRemove({_id: id}).then((todo) => {
        if(!todo) {
            res.status(404).send();
        } else {
            res.send({todo})
        }
    }).catch((e) => res.status(404).send());
});

app.patch('/todos/:id', (req, res) => {
    var id = req.params.id;
    var body = _.pick(req.body, ['text', 'completed']);

    if(!ObjectID.isValid(id)) {
        res.status(404).send();
    }

    if(_.isBoolean(body.completed) && body.completed) {
        body.completedAt = new Date().getTime();
    } else {
        body.completed = false;
        body.completedAt = null;
    }

    Todo.findByIdAndUpdate(id, {$set: body}, {new: true}).then((todo) => {
        if(!todo) {
            return res.status(400).send();
        }

        res.send({todo});
    }).catch((e) => res.status(400).send());
});
 
app.listen(3000, () => {
    console.log('Started on port 3000');
});
