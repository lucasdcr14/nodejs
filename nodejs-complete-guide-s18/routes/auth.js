const express = require('express');
const bcrypt = require('bcryptjs');
const { check, body } = require('express-validator/check');

const authController = require('../controllers/auth');
const User = require('../models/user');

const router = express.Router();

router.get('/login', authController.getLogin);

router.get('/signup', authController.getSignup);

router.post(
    '/login',
    [
        body('email')
            .isEmail()
            .withMessage('Please enter a valid email address.')
            .custom((value, { req }) => {
                return User.findOne({ email: value })
                    .then(user => {
                        if (!user) {                            
                            return Promise.reject('User not found.');
                        }
                    });
            })
            .normalizeEmail(),
        body('password')
            .isLength({ min: 5 })
            .withMessage('Password has to be valid.')
            .isAlphanumeric()
            .withMessage('Password has to be valid.')
            .custom((value, { req }) => {
                return User.findOne({ email: req.body.email })
                    .then(user => {
                        return bcrypt.compare(value, user.password)
                            .then(doMatch => {
                                if (!doMatch) {
                                    return Promise.reject('Invalid password.')
                                }
                            })
                    });
            })
            .trim(),
    ],
    authController.postLogin);

router.post(
    '/signup',
    [
        check('email')
            .isEmail()
            .withMessage('Please enter a valid email.')
            .custom((value, { req }) => {
                // if (value === 'test@test.com') {
                //     throw new Error('This email address if forbidden.');
                // }
                // return true;
                return User.findOne({ email: value })
                    .then(userDoc => {
                        if (userDoc) {
                            return Promise.reject('E-Mail exists already, please pick a different one.');
                        }
                    });
            })
            .normalizeEmail(),
        body('password', 'Please enter a password with only numbers and text and at least 5 characters.').isLength({ min: 5 }).isAlphanumeric().trim(),
        body('confirmPassword').custom((value, { req }) => {
            if (value !== req.body.password) {
                throw new Error('Passwords have to match!');
            }
            return true;
        }).trim()
    ],
    authController.postSignup);

router.post('/logout', authController.postLogout);

router.get('/reset', authController.getReset);

router.post('/reset', authController.postReset);

router.get('/reset/:token', authController.getNewPassword);

router.post('/new-password', authController.postNewPassword);

module.exports = router;
