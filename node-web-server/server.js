const express = require('express');
const hbs = require("hbs"); 
const fs = require("fs");

var app = express();

app.set('view engine', 'hbs');
app.use(express.static(__dirname + '/public'));

hbs.registerPartials(__dirname + '/views/partials');
hbs.registerHelper('getCurrentYear', () => {
    return new Date().getFullYear();
});
hbs.registerHelper('screamIt', (text) => {
    return text.toUpperCase();
});

app.use((req, res, next) => {
    var now = new Date().toString();
    var log = `${now}: ${req.method} ${req.url}`;

    console.log(log);
    fs.appendFile('server.log', log + '\n');
    next();
});

/* app.use((req, res, next) => {
    res.render('maintenance');
}) */

app.get("/", (req, res) => {
    // res.send('<h1>Hello Express!<h1>');

    /* res.send({
        name: 'Lucas',
        likes: [
            'Biking',
            'Cities'
        ]
    }) */

    res.render('home.hbs', {
        pageTitle: 'Home Page',
        pageMessage: 'Welcome to my web site'
    })
});

app.get("/about", (req, res) => {
    //res.send('About');
    res.render('about.hbs', {
        pageTitle: 'About page'
    });
});

// /bad - send back json with errorMessage

app.get("/bad", (req, res) => {
    res.send({
        message: "Unable to fulfill this page"
    })
});

app.listen(3000, () => {
    console.log('Listening on port: 3000');
})